import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import FormInterface from './components/FormInterface';
import Widget from './components/Widget';

const App = () => (
  <Router>
    <Switch>
      <Route path='/' exact component={FormInterface} />
      <Route path='/click2anything' exact component={Widget} />
    </Switch>
  </Router>
);

export default App;
