import { useState } from 'react';
import { IParams } from '../../models/interface';
import { StyledWidget } from '../../styles/StyledWidget';
import EmailIcon from '../EmailIcon ';
import WhatsappIcon from '../WhatsappIcon';

const Widget = () => {
  const [display, setDisplay] = useState<string>('none');
  const [params, setParams] = useState<IParams>({
    emailAddress: '',
    phoneNumber: '',
  });

  // eslint-disable-next-line no-restricted-globals
  const queryParams = new URLSearchParams(location.search);

  const showChannels = (): void => {
    const newParams: IParams = { ...params };

    for (let [key, value] of queryParams.entries() as any) {
      newParams[key] = value;
    }

    setParams(newParams);
    setDisplay(display => (display === 'none' ? 'inline-block' : 'none'));
  };

  return (
    <StyledWidget>
      <button id='contact-us-btn' type='button' onClick={showChannels}>
        Fale Conosco
      </button>

      <div id='channels-popup' style={{ display }}>
        {params.emailAddress && <EmailIcon emailAddress={params.emailAddress} />}
        {params.phoneNumber && <WhatsappIcon phoneNumber={params.phoneNumber} />}
      </div>
    </StyledWidget>
  );
};

export default Widget;
