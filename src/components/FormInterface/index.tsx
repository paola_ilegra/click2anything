import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { Channels } from '../../models/enum';
import { StyledFormInterface } from '../../styles/StyledFormInterface';

const FormInterface = () => {
  const [controlDisplay, setControlDisplay] = useState<any>({
    emailCheckControl: 'none',
    whatsappCheckControl: 'none',
    codeSnippetDisplay: 'none',
  });
  const { register, handleSubmit, getValues } = useForm();
  const channelsValues = getValues<Channels>([Channels.EMAIL, Channels.WHATSAPP]);
  const [codeSnippet, setCodeSnippet] = useState<string>('');

  const toggleAccordionIsActive = (event: React.FormEvent<HTMLButtonElement>, accordionPosition: number): void => {
    event.preventDefault();
    const accordions: NodeListOf<Element> = document.querySelectorAll('.accordion');
    const accordionClassList: DOMTokenList = accordions[accordionPosition].classList;

    accordionClassList.contains('is-active')
      ? accordionClassList.remove('is-active')
      : accordionClassList.add('is-active');
  };

  const handleChange = (formField: string): void => {
    if (formField === Channels.EMAIL) {
      setControlDisplay((controlDisplay: any) =>
        controlDisplay.emailCheckControl === 'none'
          ? { ...controlDisplay, emailCheckControl: 'block' }
          : { ...controlDisplay, emailCheckControl: 'none' },
      );
    }

    if (formField === Channels.WHATSAPP) {
      setControlDisplay((controlDisplay: any) =>
        controlDisplay.whatsappCheckControl === 'none'
          ? { ...controlDisplay, whatsappCheckControl: 'block' }
          : { ...controlDisplay, whatsappCheckControl: 'none' },
      );
    }
  };

  const mountCodeSnippet = (inputs: any): string => {
    const query: string = Object.keys(inputs).reduce((queryParams, key) => {
      if (!queryParams) {
        queryParams = `?${key}=${inputs[key]}`;
        return queryParams;
      }

      queryParams += `&${key}=${inputs[key]}`;
      return queryParams;
    }, '');

    // return sample: http://localhost:3000/click2anything/?emailAddress=example@example.com&phoneNumber=+5551999999999
    return `<iframe src="http://localhost:3000/click2anything/${query}"></iframe>`;
  };

  const onSubmit = (): void => {
    // validate if user choose at least one channel to use:
    const noChannelChecked: boolean = Object.values(channelsValues).every(value => !value);

    if (noChannelChecked) {
      alert('Deve haver pelo menos um canal escolhido');
      return;
    }

    const formValue: any = getValues();
    const inputs: any = Object.keys(formValue).reduce((result: any, key: string) => {
      if (typeof formValue[key] === 'string') {
        result[key] = formValue[key];
      }

      return result;
    }, {});

    setCodeSnippet(mountCodeSnippet(inputs));
    // shows code snippet at the end of page:
    setControlDisplay({ ...controlDisplay, codeSnippetDisplay: 'block' });
  };

  return (
    <StyledFormInterface>
      <aside className='menu'>
        <p className='menu-label'>Divider</p>
        <ul className='menu-list'>
          <li>
            <a href='/'>Item</a>
          </li>
          <li>
            <a href='/'>Item</a>
          </li>
          <li>
            <a href='/'>Item</a>
          </li>
        </ul>
        <p className='menu-label'>Divider</p>
        <ul className='menu-list'>
          <li>
            <a href='/'>Item</a>
          </li>
          <li>
            <a href='/'>Item</a>
          </li>
          <li>
            <a href='/'>Item</a>
          </li>
          <li>
            <a href='/'>Item</a>
          </li>
        </ul>
      </aside>

      <form className='form-content' onSubmit={handleSubmit(onSubmit)}>
        <nav className='breadcrumb' aria-label='breadcrumbs'>
          <ul>
            <li>
              <a href='/'>Página Inicial</a>
            </li>
            <li className='is-active'>
              <a href='/' aria-current='page'>
                Customize seu atendimento
              </a>
            </li>
          </ul>
        </nav>

        <p className='form-title'>Customize seu atendimento</p>

        <p className='form-subtitle'>Prepare-se para conversar com seus clientes</p>
        <p className='form-text'>
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Necessitatibus, omnis expedita eligendi, rerum optio
          dignissimos vel perferendis quos quas eos fuga id mollitia, quaerat nemo commodi hic veritatis illum iusto.
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Atque sapiente accusantium repellat nostrum!
          Veritatis possimus obcaecati omnis. Ipsa sit natus ab quos! Quo eos, corrupti maiores dolore nihil nobis
          incidunt.
        </p>

        <section className='accordions'>
          <article className='accordion'>
            <div className='accordion-header toggle'>
              Identificação do Usuário
              <button
                className='toggle'
                aria-label='toggle'
                onClick={(e: React.FormEvent<HTMLButtonElement>) => toggleAccordionIsActive(e, 0)}
              ></button>
            </div>
            <div className='accordion-body' id='accordion-body'>
              <div className='accordion-content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
            </div>
          </article>

          <article className='accordion'>
            <div className='accordion-header toggle'>
              Mensagem inicial (opcional)
              <button
                className='toggle'
                aria-label='toggle'
                onClick={(e: React.FormEvent<HTMLButtonElement>) => toggleAccordionIsActive(e, 1)}
              ></button>
            </div>
            <div className='accordion-body' id='accordion-body'>
              <div className='accordion-content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
            </div>
          </article>

          <article className='accordion'>
            <div className='accordion-header'>
              Escolha dos canais
              <button
                className='toggle'
                aria-label='toggle'
                onClick={(e: React.FormEvent<HTMLButtonElement>) => toggleAccordionIsActive(e, 2)}
              ></button>
            </div>
            <div className='accordion-body' id='accordion-body'>
              <div className='accordion-content'>
                <div className='field'>
                  <p className='control'>
                    <input
                      className='checkradio'
                      id={Channels.EMAIL}
                      type='checkbox'
                      name={Channels.EMAIL}
                      ref={register}
                      onChange={() => handleChange(Channels.EMAIL)}
                    />
                    <label htmlFor={Channels.EMAIL}>E-mail</label>
                  </p>
                </div>

                <div className='email-check-control' style={{ display: controlDisplay.emailCheckControl }}>
                  <div className='field'>
                    <p className='control'>
                      <input
                        className='input'
                        type='text'
                        name='emailAddress'
                        ref={register}
                        placeholder='example@example.com'
                      />
                    </p>
                  </div>
                </div>

                <div className='field'>
                  <p className='control'>
                    <input
                      className='checkradio'
                      id={Channels.WHATSAPP}
                      type='checkbox'
                      name={Channels.WHATSAPP}
                      ref={register}
                      onChange={() => handleChange(Channels.WHATSAPP)}
                    />
                    <label htmlFor={Channels.WHATSAPP}>WhatsApp</label>
                  </p>
                </div>

                <div className='whatsapp-check-control' style={{ display: controlDisplay.whatsappCheckControl }}>
                  <div className='field'>
                    <p className='control'>
                      <input
                        className='input'
                        type='text'
                        name='phoneNumber'
                        ref={register}
                        placeholder='5551999999999'
                      />
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </article>

          <article className='accordion'>
            <div className='accordion-header'>
              Mensagem final (opcional)
              <button
                className='toggle'
                aria-label='toggle'
                onClick={(e: React.FormEvent<HTMLButtonElement>) => toggleAccordionIsActive(e, 3)}
              ></button>
            </div>
            <div className='accordion-body' id='accordion-body'>
              <div className='accordion-content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
            </div>
          </article>

          <article className='accordion'>
            <div className='accordion-header'>
              Detalhes da customização
              <button
                className='toggle'
                aria-label='toggle'
                onClick={(e: React.FormEvent<HTMLButtonElement>) => toggleAccordionIsActive(e, 4)}
              ></button>
            </div>
            <div className='accordion-body' id='accordion-body'>
              <div className='accordion-content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
            </div>
          </article>
        </section>

        <button onClick={handleSubmit(onSubmit)}>Pronto</button>
        {/* this div style will be overwritten, it is temporary */}
        <div
          style={{
            width: '648px',
            height: '100px',
            border: '1px solid black',
            margin: '40px',
            display: controlDisplay.codeSnippetDisplay,
          }}
        >
          <p>{codeSnippet}</p>
        </div>
      </form>
    </StyledFormInterface>
  );
};

export default FormInterface;
