import styled from 'styled-components';

export const StyledVoiceIcon = styled.div`
  #voice-icon {
    background: none;
    border: none;
    margin-right: 10px;
    position: fixed;
    top: 27px;
  }

  #voice-icon:focus {
    outline: none;
  }

  #phone-input-form {
    width: 168px;
    height: 52px;
    padding: 8px;
    left: 50px;
    position: relative;
    background: #fff;
    border: 1px solid #000;
  }

  #phone-input-form:after,
  #phone-input-form:before {
    right: 100%;
    top: 50%;
    border: solid transparent;
    content: '';
    height: 0;
    width: 0;
    position: absolute;
    pointer-events: none;
  }

  #phone-input-form:after {
    border-color: rgba(255, 255, 255, 0);
    border-right-color: #fff;
    border-width: 10px;
    margin-top: -10px;
  }

  #phone-input-form:before {
    border-color: rgba(0, 0, 0, 0);
    border-right-color: #000;
    border-width: 11px;
    margin-top: -11px;
  }

  #call-me-btn {
    margin: 6px;
  }
`;
