import styled from 'styled-components';

export const StyledWidget = styled.div`
  width: 250px;

  #contact-us-btn {
    width: 100px;
    height: 30px;
    position: fixed;
    top: 24px;
    left: 105px;
  }

  #channels-popup {
    width: 75px;
    height: 180px;
    padding: 20px;

    position: relative;
    background: white;
    border: 1px solid #000;
    padding: 0.5rem;
  }

  #channels-popup:after,
  #channels-popup:before {
    left: 100%;
    top: 50%;
    border: solid transparent;
    content: '';
    height: 0;
    width: 0;
    position: absolute;
    pointer-events: none;
  }

  #channels-popup:after {
    border-color: rgba(136, 183, 213, 0);
    border-left-color: white;
    border-width: 18px;
    margin-top: -70px;
  }

  #channels-popup:before {
    border-color: rgba(0, 0, 0, 0);
    border-left-color: #000;
    border-width: 19px;
    margin-top: -71px;
  }

  .whatsapp-btn {
    color: #01e675;
  }
`;
