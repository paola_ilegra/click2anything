import styled from 'styled-components';

export const StyledFormInterface = styled.main`
  display: flex;
  flex-direction: row;

  .menu {
    width: 20%;
    height: 100vh;
  }

  .form-content {
    width: 60%;
    padding: 36px 190px 34px 40px;
  }

  .form-title {
    font-size: 40px;
    font-weight: 300;
  }

  .form-subtitle {
    font-size: 24px;
    font-weight: 300;
  }

  .form-text {
    margin-top: 18px;
    margin-bottom: 32px;
  }

  .accordions {
    margin-top: 32px;
    width: 100%;
  }

  .accordions .accordion .accordion-header {
    margin: 0 !important;
    background-color: #f7f7f9 !important;
  }

  .accordions .accordion:not(:last-child) {
    border-bottom: 1px solid #d1d1d7;
    border-radius: 0 !important;
  }

  .email-check-control,
  .whatsapp-check-control {
    margin-bottom: 14px;
  }
`;
