export interface IParams {
  [key: string]: string;
  emailAddress: string;
  phoneNumber: string;
}
