# Click2Anything

### Para rodar o projeto

- Conectar a VPN;
- Clonar este repositório `git clone https://paola_ilegra@bitbucket.org/paola_ilegra/click2anything.git`;
- Instalar as dependências `yarn install`;
- Rodar o seguinte comando para subir o projeto e abrir no browser: `npm start`.

#### Para testar o widget

- O projeto irá rodar como padrão na URL (http://localhost:3000) que está configurada como rota raiz para a aplicação e, é a rota responsável por renderizar o formulário inicial, onde é possível escolher os canais e configurar o widget;
- O widget em si, está configurado para ser renderizado na URL (http://localhost:3000/click2anything), porém só irá funcionar os canais de comunicação se passarmos os query parameters junto à rota, que podem ser facilmente configurados no formulário inicial;
- Após a escolha dos canais e o envio do formulário, (clicando no botão "Pronto"), é possível visualizar o code snippet ao final da página, que pode ser copiado e colado para utilização;
- Para colocar o widget à prova, há duas maneiras:

  1. Pode-se criar um arquivo nomeado index.html, com as configurações abaixo, e colar o snippet inteiro no body do arquivo, exemplo:
  
~~~html
  <!DOCTYPE html>
  <html lang="en">
    <head>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta charset="UTF-8" />
      <title>Test</title>
    </head>

    <body>
      <!-- colar code snippet aqui -->
    </body>
  </html>
~~~

  2. Pode-se copiar e colar apenas o link que está como atributo src da tag iframe "http://localhost:3000/click2anything?..." e colar em uma nova aba direto no browser, assim irá renderizar apenas o widget.
